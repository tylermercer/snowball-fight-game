/* eslint-disable */

const path = require('path');

module.exports = {
	entry: {
		"screen": "./src/screen/screen.js",
		"controller": "./src/controller/controller.js"
	},
	output: {
		filename: "./public/build/[name].bundle.js",
	},
	resolve: {
		alias: {
			"phaser": path.resolve(__dirname, "src/screen/custom-phaser"),
		},
	},
	devtool: "source-map",
};
