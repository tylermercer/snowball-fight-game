#!/bin/bash

# Change these parameters to do what you want

file=player/billy_0_red.png

# size of each sprite
w=128
h=$w

# offset in sprites (e.g. this does first row, third column)
x=2
y=0

frames=6
direction=v

# ^^^ End of params ^^^

path=../public/sprites/"$file"
if ! [ -e "$path" ]; then
	echo "Bad file: $path" >&2
	exit 1
fi

# Code below draws from https://stackoverflow.com/questions/56040013/convert-sprite-sheet-to-gif-animation

viewport=''
case $direction in
horizontal|horiz|h) viewport=%[fx:$frames*$w]x$h ;;
vertical|vert|v)    viewport=${w}x%[fx:$frames*$h] ;;
*) echo "bad direction"; exit 1 ;;
esac

convert -dispose background "$path" \
-set option:distort:viewport "$viewport" \
-distort affine "$((x*w)),$((y*h)) 0,0" \
-crop ${w}x${h} +adjoin +repage -adjoin -loop 0 -delay 1 result.gif &&

eog result.gif # open a viewer of the result

# result is stored in result.gif
