#!/bin/sh

color="blue"

montage -background none -geometry +0+0 -tile 6x \
	player/billy_0_$color.png \
	player/billy_1_$color.png \
	player/billy_2_$color.png \
	player/billy_3_$color.png \
	\
	\( -clone 0 -crop 128x128+0+0 \) \
	\( -clone 0 -crop 128x128+0+128 \) \
	\( -clone 0 -crop 128x128+0+256 \) \
	\( -clone 0 -crop 128x128+0+384 \) \
	\( -clone 0 -crop 128x128+0+512 \) \
	\( -clone 0 -crop 128x128+0+640 \) \
	\
	\( -clone 0 -crop 128x128+256+0 \) \
	\( -clone 0 -crop 128x128+256+128 \) \
	\( -clone 0 -crop 128x128+256+256 \) \
	\( -clone 0 -crop 128x128+256+384 \) \
	\( -clone 0 -crop 128x128+256+512 \) \
	\( -clone 0 -crop 128x128+256+640 \) \
	\
	\( -clone 0 -crop 128x128+512+0 \) \
	\( -clone 0 -crop 128x128+512+128 \) \
	\( -clone 0 -crop 128x128+512+256 \) \
	\( -clone 0 -crop 128x128+512+384 \) \
	\( -clone 0 -crop 128x128+512+512 \) \
	\( -clone 0 -crop 128x128+512+640 \) \
	\
	\( -clone 0 -crop 128x128+768+0 \) \
	\( -clone 0 -crop 128x128+768+128 \) \
	\( -clone 0 -crop 128x128+768+256 \) \
	\( -clone 0 -crop 128x128+768+384 \) \
	\( -clone 0 -crop 128x128+768+512 \) \
	\( -clone 0 -crop 128x128+768+640 \) \
	\
	\( -clone 0 -crop 128x128+0+0 \) \
	\( -clone 1 -crop 128x128+0+0 \) \
	\( -clone 2 -crop 128x128+0+0 \) \
	\( -clone 3 -crop 128x128+0+0 \) \
	\
	-delete 3 \
	-delete 2 \
	-delete 1 \
	-delete 0 \
	player/billy_tutorial.png
