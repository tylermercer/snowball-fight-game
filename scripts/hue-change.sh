#!/bin/bash

# A script for replacing a specified hue. See
# http://www.imagemagick.org/Usage/color_mods/#modulate_hue
# to understand the transform parameter.

in_file="$1"
in_hue=$2
in_tolerance=$3
out_hue=$4
out_file="$5"

if [ -z "$1" -o -z "$2" -o -z "$3" -o -z "$4" -o -z "$5" ]; then
	echo "USAGE: $0 in.png in-hue(0.00-1.00) in-tolerance(0.00-1.00) out_hue(0.00-1.00) out.png"
	exit 1
fi

if ! [ -f "$in_file" ]; then
	echo "No such file: $in_file"
	exit 1
fi

# The IM function
func="
	diff = abs(u.hue - $in_hue);
	weight = ($in_tolerance - diff) / $in_tolerance;
	weight = max(0, min(1, weight));
	weight * v + (1 - weight) * u"


# Copy the picture and set the hue to the given output hue
convert "$in_file" -colorspace HSL \
		-channel R -evaluate set 100% -evaluate multiply $out_hue +channel \
		-colorspace sRGB temp.png
echo "Hue-changed copy created..."

# Now perform the masking operation
convert "$in_file" temp.png \
	-fx "$func" "$out_file"

# Remove the temp file
rm temp.png

echo "Done"
notify-send "Finished!" "Hue adjustment complete."

xdg-open "$out_file"
