#!/bin/bash

# The offsets for facing S, SW, etc.
amt_s=20
amt_sw=24
amt_w=28
amt_nw=24
amt_n=20
amt_ne=24
amt_e=28
amt_se=24

y=768

# The x-offsets of the columns for facing S, SW, etc.
x_s=0
x_sw=128
x_w=256
x_nw=384
x_n=512
x_ne=640
x_e=768
x_se=896

function adjustImage () {
	echo $1 $((y + amt_sw))
	convert \
		\( $1 \( -size 1024x256 -geometry +0+768 xc:none \) -compose copy -composite \) \
		\( $1 -crop 128x256+$x_sw+$y \) \
		-geometry +$x_sw+$((y + amt_sw)) \
		-compose Over -composite \
		\( $1 -crop 128x256+$x_s+$y \) \
		-geometry +$x_s+$((y + amt_s)) \
		-compose Over -composite \
		\( $1 -crop 128x256+$x_w+$y \) \
		-geometry +$x_w+$((y + amt_w)) \
		-compose Over -composite \
		\( $1 -crop 128x256+$x_nw+$y \) \
		-geometry +$x_nw+$((y + amt_nw)) \
		-compose Over -composite \
		\( $1 -crop 128x256+$x_n+$y \) \
		-geometry +$x_n+$((y + amt_n)) \
		-compose Over -composite \
		\( $1 -crop 128x256+$x_ne+$y \) \
		-geometry +$x_ne+$((y + amt_ne)) \
		-compose Over -composite \
		\( $1 -crop 128x256+$x_e+$y \) \
		-geometry +$x_e+$((y + amt_e)) \
		-compose Over -composite \
		\( $1 -crop 128x256+$x_se+$y \) \
		-geometry +$x_se+$((y + amt_se)) \
		-compose Over -composite \
		$1
}

for file in player/billy_[0-2]_*.png
do
	adjustImage $file
	echo "Done with $file"
done

echo "Done"
