"use strict";

const Phaser = require("phaser");

// *** Utility classes ***

// Dir class, defines cardinal directions and related utilities
const Dir = {};
Dir.S = "s";
Dir.SW = "sw";
Dir.W = "w";
Dir.NW = "nw";
Dir.N = "n";
Dir.NE = "ne";
Dir.E = "e";
Dir.SE = "se";
const dirToVecMap = {
	[Dir.S]:  [ 0,  1],
	[Dir.SW]: [-1,  1],
	[Dir.W]:  [-1,  0],
	[Dir.NW]: [-1, -1],
	[Dir.N]:  [ 0, -1],
	[Dir.NE]: [ 1, -1],
	[Dir.E]:  [ 1,  0],
	[Dir.SE]: [ 1,  1],
};
Dir.getDirectionAsVector = function(/*String*/  dir, /*Boolean*/ normalize=true) {
	var vector = new Phaser.Point(...dirToVecMap[dir]);
	if (normalize) vector.normalize();
	return vector;
};

Dir.getClosestCardinal = function(/*Phaser.Point*/ vector) {
	if (Math.abs(vector.x)*Constants.UNIT_ASPECT_RATIO >= Math.abs(vector.y)) {
		// East or West
		return (vector.x >= 0) ? Dir.E : Dir.W;
	} else {
		// North or South
		return (vector.y >= 0) ? Dir.S : Dir.N;
	}
};

Dir.fromDegrees = function(degrees) {
	if (degrees < -180+22.5) return Dir.W;
	if (degrees < -90 -22.5) return Dir.NW;
	if (degrees < -90 +22.5) return Dir.N;
	if (degrees <     -22.5) return Dir.NE;
	if (degrees <     +22.5) return Dir.E;
	if (degrees <  90 -22.5) return Dir.SE;
	if (degrees <  90 +22.5) return Dir.S;
	if (degrees <  180-22.5) return Dir.SW;
	else                     return Dir.W;
};

// Constants class, contains game-wide constants
const Constants = {};
Constants.ANIM_SPEED = 20;
Constants.SLING_SPEED = 1200;
Constants.SLING_RECOIL = 85;
Constants.SLING_JITTER = 0.04;
Constants.SNOWBALL_MASS = 1;
Constants.SNOW_LEVEL_1 = 5;
Constants.SNOW_LEVEL_2 = 12;
Constants.SNOW_LEVEL_DEFEATED = 21;
Constants.SNOWBALL_IMPACT_FORCE = 7;
Constants.PLAYER_DRAG = {x: 30, y: 40};
Constants.PLAYER_DRAG_FLATTENED = {x: 50, y: 56};
Constants.UNIT_ASPECT_RATIO = .5;
Constants.PLAYER_SPEED_MAX = 350;
Constants.ROLL_SPEED_MAX = 200;
Constants.ROLL_SPEED_MIN = 50;
Constants.SNOWBALL_SHADOW_RISE_HEIGHT = 36;
Constants.GAMES_PER_AD = 3;

// Freeze all constants so they cannot be changed
Object.freeze(Dir);
Object.freeze(Constants);

module.exports = {
	Dir: Dir,
	Constants: Constants,
};
