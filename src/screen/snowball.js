"use strict";

const Shadows = require("./shadow");
const GameUtils = require("./gameutils");
const Utils = require("../common/utils");
const Dir = Utils.Dir;
const Constants = Utils.Constants;

const offsets = {
	[Dir.S]:  { x: 17,  y: 35},
	[Dir.SW]: { x: 16,  y: 31},
	[Dir.W]:  { x: 32,  y: 33},
	[Dir.NW]: { x: 70,  y: 10},
	[Dir.N]:  { x: 95,  y: 23},
	[Dir.NE]: { x: 100, y: 38},
	[Dir.E]:  { x: 78,  y: 47},
	[Dir.SE]: { x: 37,  y: 55},
};

class Snowball {
	constructor(/*Player*/ sourcePlayer, /*Phaser.Point*/ position, /*String*/ direction) {
		var sprite = Snowball.spriteGroup.create(0, 0, "bullet");
		this.direction = direction||sourcePlayer.direction;
		sprite.body.velocity = Dir.getDirectionAsVector(this.direction, false);

		let jitter = Constants.SLING_JITTER;
		if (sourcePlayer) {
			jitter *= sourcePlayer.getJitterFactor();
		}
		const game = Snowball.spriteGroup.game;
		sprite.body.velocity.x += game.rnd.realInRange(-jitter, jitter);
		sprite.body.velocity.y += game.rnd.realInRange(-jitter, jitter);
		sprite.body.velocity.setMagnitude(Constants.SLING_SPEED);

		if (sourcePlayer) {
			if (position) sprite.position = position;
			else sprite.position = sourcePlayer.position.clone();

			var offset = offsets[this.direction];
			sprite.centerX += offset.x;
			sprite.centerY += offset.y;

			sprite.body.velocity.add(sourcePlayer.velocity.x, sourcePlayer.velocity.y);
		} else if (position) {
			sprite.position = position;
		}

		sprite.animations.add("spin", [0, 1, 2, 3, 4, 5]);
		sprite.play("spin", Constants.ANIM_SPEED, true);
		sprite.body.bounce.x = 1;
		sprite.body.bounce.y = 1;

		GameUtils.makeCircleBounds(sprite.body, 64, sprite.width * .8);

		Shadows.addFor(sprite);

		sprite.sn_source = sourcePlayer;
		sprite.sn_type = "Snowball";
		sprite.sn_snowball = this;
		this.sprite = sprite;
		sprite.checkWorldBounds = true;
		sprite.events.onOutOfBounds.add(function(sprite) {
			sprite.kill();
			sprite.destroy();
		});
	}

	kill() {
        //IDEA: Add particle animation?
		this.sprite.kill();
		this.sprite.destroy();
	}

	elevateShadow() {
		this.sprite.sn_shadow.sn_elevation = Constants.SNOWBALL_SHADOW_RISE_HEIGHT;
	}
}

Snowball.spritesheetName = "";
Snowball.spriteGroup = null;

module.exports = Snowball;
