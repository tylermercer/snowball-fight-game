"use strict";
/*eslint no-undef: "error"*/

const TextUtils = {};

/**
 * Fixes the given style hash so it doesn't trigger bugs found in
 * Phaser.Text.prototype.setStyle.
 * @param  {Object} style A style hash
 */
TextUtils.fixStyle = function(style) {
	style.align || (style.align = "");
	style.boundsAlignH || (style.boundsAlignH = "");
	style.boundsAlignV || (style.boundsAlignV = "");
	return style;
};

const DEFAULT_STYLE = {
	fontSize: "32px",
	font: "Ravi Prakash",
	wordWrap: false,
	align: "center",
	fill: "#f9f9eb",
	stroke: "#222222",
	strokeThickness: 2,
	// This array contains the arguments for Phaser.Text.prototype.setShadow:
	shadow: [0, 4, "rgba(0,0,0,.5)", 10],
};

/**
 * Creates a new Phaser.Text object with default styling (which may be
 * overridden by the `style` parameter).
 * @param  {Phaser.Game} game   The Phaser game
 * @param  {String}		 string The text to show
 * @param  {Object}		 style  (optional) A hash of style attributes to apply.
 */
TextUtils.makeText = function(game, string, style) {
	// Make a new style hash from the default style and the given style options
	const styleHash = Object.assign({}, DEFAULT_STYLE, style);

	const text = game.add.text(0, 0, string, TextUtils.fixStyle(styleHash));
	if (styleHash.shadow) {
		text.setShadow(...styleHash.shadow);
	}
	return text;
};

module.exports = TextUtils;
