"use strict";

const MapManager = require("./mapmanager");
const Shadows = require("./shadow");
const Phaser = require("phaser");
const Player = require("./player");
const Snowball = require("./snowball");
const CollisionsHandler = require("./collisions");

class World {
	constructor(game) {
		this.game = game;

		this.hasWalls = false;
		this.sprites = null;
		this.players = null;
	}

	create() {
		this.clear();

		MapManager.init(this.game); // Always init MapManager before everything else

		this.game.scale.onSizeChange.removeAll(this);
		this.game.scale.onSizeChange.add(function() {
			MapManager.update();
		}, this);

		Shadows.init(this.game); // Always init Shadows before player and snowball spritegroup
		const sprites = this.game.add.physicsGroup(Phaser.Physics.ARCADE);
		Player.spriteGroup = sprites;
		Snowball.spriteGroup = sprites;
		MapManager.spriteGroup = sprites;
		this.sprites = sprites;

		// Initialize ARCADE Physics
		this.game.physics.startSystem(Phaser.Physics.ARCADE);

		// Init players map:
		this.players = {};

		// Init collisionsHandler
		this.collisionsHandler = new CollisionsHandler(this.players, this.game);
		this.collisionsHandler.onPlayerHit = (player) => this.onPlayerHit(player);
		// Reset event listener:
		this.onPlayerHit = () => {};
	}

	addWalls() {
		if (!this.hasWalls) {
			MapManager.buildWalls();
			this.hasWalls = true;
		}
	}

	addPlayer(id, name, team) {
		if (this.players[id]) {
			throw new Error("A player with that ID already exists!");
		}

		// Initialize new player
		const player = new Player(team, id, name);

		// Position the new player
		let pos;
		if (this.hasWalls) {
			pos = MapManager.getPointInArena(
				player.sprite.body.width + player.sprite.body.offset.x,
				player.sprite.body.height + player.sprite.body.offset.y
			);
		} else {
			pos = this.getRandomPointInWorld();
		}
		player.setPosition(pos.x, pos.y);

		this.players[id] = player;
		return player;
	}

	getPlayerForID(id) {
		return this.players[id];
	}

	getRandomPointInWorld() {
		const game = this.game;
		return {
			x: game.rnd.integerInRange(0, game.world.width),
			y: game.rnd.integerInRange(0, game.world.height),
		};
	}

	update() {
		const players = this.players;
		const sprites = this.sprites;

		for (let id in players) {
			const player = players[id];

			if (player.isDefeated) {
				player.updateRoll();
			}
		}

		// Sort sprites:
		const rankSprite = function(a) {
			return a.y + (a.body ? a.body.offset.y : 0)
					+ (a.sn_foreground || 0)
					+ (a.sn_isKnockedDown?-32:0);
		};
		sprites.customSort(function(a, b) {
			let yA = rankSprite(a), yB = rankSprite(b);
			return (yA > yB) ? 1 : (yA === yB) ? 0 : -1;
		});

		this.collisionsHandler.collide(sprites);
	}

	clear() {
		if (this.sprites) {
			this.sprites.destroy();
			this.sprites = null;
		}
		this.players = {};
		this.hasWalls = false;
	}

	//======================================================================
	//			Event listeners (abstract)

	onPlayerHit() {}
}

module.exports = World;
