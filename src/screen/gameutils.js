"use strict";

const Constants = require("../common/utils").Constants;

const GameUtils = {
	makeCircleBounds: function(body, height, diameter) {
		const origWidth = body.width;

		body.height = diameter * Constants.UNIT_ASPECT_RATIO;
		body.width = diameter;

		body.offset.x = (origWidth - diameter) / 2;
		body.offset.y = height - body.height/2;
	},
};

module.exports = GameUtils;
