"use strict";

const Phaser = require("phaser");
const Snowball = require("./snowball");
const Nametag = require("./nametag");
const Shadows = require("./shadow");
const AudioUtils = require("./audioutils");
const GameUtils = require("./gameutils");
const Utils = require("../common/utils");
const Dir = Utils.Dir;
const Constants = Utils.Constants;

class Player {
	constructor(/*String*/ team, /*Number*/ id, /*String*/ playerName) {
        // Throw an error if the spritegroup is not yet defined
		if(!Player.spriteGroup) throw new Error("Player.spriteGroup must be defined before Players can be instantiated");

		this.team = team;
		this.id = id;
        // Initialize sprite in spriteGroup
		this.sprite = Player.spriteGroup.create(0, 0, "billy_0_" + team);
		this.body = this.sprite.body;

        // Initialize first half of throw animations
		this.sprite.animations.add(Dir.S,  [0, 8,  16, 24, 32]);
		this.sprite.animations.add(Dir.SW, [1, 9,  17, 25, 33]);
		this.sprite.animations.add(Dir.W,  [2, 10, 18, 26, 34]);
		this.sprite.animations.add(Dir.NW, [3, 11, 19, 27, 35]);
		this.sprite.animations.add(Dir.N,  [4, 12, 20, 28, 36]);
		this.sprite.animations.add(Dir.NE, [5, 13, 21, 29, 37]);
		this.sprite.animations.add(Dir.E,  [6, 14, 22, 30, 38]);
		this.sprite.animations.add(Dir.SE, [7, 15, 23, 31, 39]);

        //Initialize second half of throw animations
		this.sprite.animations.add(Dir.S + 2,  [40, 0]);
		this.sprite.animations.add(Dir.SW + 2, [41, 1]);
		this.sprite.animations.add(Dir.W + 2,  [42, 2]);
		this.sprite.animations.add(Dir.NW + 2, [43, 3]);
		this.sprite.animations.add(Dir.N + 2,  [44, 4]);
		this.sprite.animations.add(Dir.NE + 2, [45, 5]);
		this.sprite.animations.add(Dir.E + 2,  [46, 6]);
		this.sprite.animations.add(Dir.SE + 2, [47, 7]);

        //Initialize flatten backward animations
		this.sprite.animations.add(Dir.S  + "flatten_0", [48]);
		this.sprite.animations.add(Dir.SW + "flatten_0", [49]);
		this.sprite.animations.add(Dir.W  + "flatten_0", [50]);
		this.sprite.animations.add(Dir.NW + "flatten_0", [51]);
		this.sprite.animations.add(Dir.N  + "flatten_0", [52]);
		this.sprite.animations.add(Dir.NE + "flatten_0", [53]);
		this.sprite.animations.add(Dir.E  + "flatten_0", [54]);
		this.sprite.animations.add(Dir.SE + "flatten_0", [55]);

        //Initialize flatten forward animations
		this.sprite.animations.add(Dir.S  + "flatten_1", [56]);
		this.sprite.animations.add(Dir.SW + "flatten_1", [57]);
		this.sprite.animations.add(Dir.W  + "flatten_1", [58]);
		this.sprite.animations.add(Dir.NW + "flatten_1", [59]);
		this.sprite.animations.add(Dir.N  + "flatten_1", [60]);
		this.sprite.animations.add(Dir.NE + "flatten_1", [61]);
		this.sprite.animations.add(Dir.E  + "flatten_1", [62]);
		this.sprite.animations.add(Dir.SE + "flatten_1", [63]);

		//Initialize wobble animations
		this.sprite.animations.add(Dir.S  + "wobble", [64, 72, 80, 88, 96,  104, 0]);
		this.sprite.animations.add(Dir.SW + "wobble", [65, 73, 81, 89, 97,  105, 1]);
		this.sprite.animations.add(Dir.W  + "wobble", [66, 74, 82, 90, 98,  106, 2]);
		this.sprite.animations.add(Dir.NW + "wobble", [67, 75, 83, 91, 99,  107, 3]);
		this.sprite.animations.add(Dir.N  + "wobble", [68, 76, 84, 92, 100, 108, 4]);
		this.sprite.animations.add(Dir.NE + "wobble", [69, 77, 85, 93, 101, 109, 5]);
		this.sprite.animations.add(Dir.E  + "wobble", [70, 78, 86, 94, 102, 110, 6]);
		this.sprite.animations.add(Dir.SE + "wobble", [71, 79, 87, 95, 103, 111, 7]);

		//Initialize defeated roll animations
		this.sprite.animations.add(Dir.N + "roll", [0, 1, 2, 3, 4, 5, 6, 7]);
		this.sprite.animations.add(Dir.W + "roll", [8, 9, 10, 11, 12, 13, 14, 15]);
		this.sprite.animations.add(Dir.S + "roll", [7, 6, 5, 4, 3, 2, 1, 0]);
		this.sprite.animations.add(Dir.E + "roll", [15, 14, 13, 12, 11, 10, 9, 8]);

		this.direction = Dir.S;
		this.sprite.play(this.direction);
		this.sprite.animations.stop();
		this.sprite.body.drag.x = Constants.PLAYER_DRAG.x;
		this.sprite.body.drag.y = Constants.PLAYER_DRAG.y;

		Nametag.addTo(this.sprite, playerName);

		GameUtils.makeCircleBounds(this.body, 102, 50);

		this.body.bounce.x = 0.7;
		this.body.bounce.y = 0.7;
		this.body.maxVelocity.x = 500;
		this.body.maxVelocity.y = 500;

		this.body.collideWorldBounds = true;

		this.sprite.sn_name = "Player " + id;
		this.sprite.sn_type = "Player";
		this.sprite.sn_player_id = id;

		Shadows.addFor(this.sprite);

        //Set accumulated snow to 0;
		this.snowMass = 0;
		this.snowLevel = 0;

		// Default value
		this.lastThrowTime = 0;
	}

	snowballImpact(/*String*/ dir, /*Point*/ vel) {
		var relativeVel = Phaser.Point.subtract(vel, this.body.velocity);
		if (!this.isDefeated) {
			this.body.velocity.x += relativeVel.x * 0.1;
			this.body.velocity.y += relativeVel.y * 0.1;
			this.snowMass += Constants.SNOWBALL_MASS;
			this.updateSnowLevel();

			if (relativeVel.getMagnitude() > Constants.SLING_SPEED * 1.35) {
				this.flatten(dir);
			} else {
				this.wobble();
			}
		} else {
			this.body.velocity.x += relativeVel.x * 0.05;
			this.body.velocity.y += relativeVel.y * 0.05;
		}
	}

	flatten(/*String*/ dir) {
		if (this.isDefeated) return;
		this.isKnockedDown = true;
		this.sprite.sn_isKnockedDown = true;
		this.sprite.body.drag.x = Constants.PLAYER_DRAG_FLATTENED.x;
		this.sprite.body.drag.y = Constants.PLAYER_DRAG_FLATTENED.y;
		var side = Dir.getDirectionAsVector(dir).dot(Dir.getDirectionAsVector(this.direction));

		var complete = function() {
			this.sprite.play(this.direction);
			this.sprite.animations.stop();
			this.isKnockedDown = false;
			this.sprite.sn_isKnockedDown = false;
			this.sprite.body.drag.x = Constants.PLAYER_DRAG.x;
			this.sprite.body.drag.y = Constants.PLAYER_DRAG.y;
		};

		if (side <= 0) {
			this.sprite.play(this.direction + "flatten_0", 1).onComplete.add(complete, this);
		} else {
			this.sprite.play(this.direction + "flatten_1", 1).onComplete.add(complete, this);
		}
	}

	wobble() {
		if (this.isDefeated) return;
		this.sprite.play(this.direction + "wobble", Constants.ANIM_SPEED);
	}

	updateSnowLevel() {
		if (this.isDefeated) return;

		if (this.snowLevel === 2 && this.snowMass >= Constants.SNOW_LEVEL_DEFEATED) {
			this.sprite.loadTexture("billy_3_" + this.team);
			this.snowLevel = 3;
			this.sprite.play("Defeated", Constants.ANIM_SPEED, true, false);
			this.isDefeated = true;
			this.body.maxVelocity.x = Constants.ROLL_SPEED_MAX;
			this.body.maxVelocity.y = Constants.ROLL_SPEED_MAX;
			this.body.drag *= 0.5;

			// Update size of collision body
			let bodyCenterX = this.body.offset.x + this.body.width / 2;
			let bodyCenterY = this.body.offset.y + this.body.height / 2;
			this.body.width *= 1.5;
			this.body.height *= 1.5;
			this.body.offset.x = bodyCenterX - this.body.width / 2;
			this.body.offset.y = bodyCenterY - this.body.height / 2;
			// Update the shadow to match the collision body.
			Shadows.updateFor(this.sprite);

			this.updateRoll();

		} else if (this.snowLevel === 1 && this.snowMass >= Constants.SNOW_LEVEL_2) {
			this.sprite.loadTexture("billy_2_" + this.team);
			this.snowLevel = 2;

		} else if (this.snowLevel === 0 && this.snowMass >= Constants.SNOW_LEVEL_1) {
			this.sprite.loadTexture("billy_1_" + this.team);
			this.snowLevel = 1;
		}
	}

    // Sets the Player's position in World coordinates
	setPosition(/*Number*/ x, /*Number*/ y) {
		this.sprite.position.set(x, y);
	}

	get position() {
		return this.sprite.position;
	}

	get velocity() {
		return this.body.velocity;
	}
    // Changes sprite to face a direction
	setDirection(/*String*/ dir) {
		if (this.throwing||this.isKnockedDown||this.isDefeated) return; // Do nothing if throwing, flattened, or defeated
		this.direction = dir;
		this.sprite.play(this.direction);
		this.sprite.animations.stop();
	}

    // First half of snowball slinging, called externally
	slingSnowball() {
		if (this.isKnockedDown || this.isDefeated) return; // Do nothing if knocked down
		this.throwing = true;
		this.sprite.animations.play(this.direction, Constants.ANIM_SPEED, false).onComplete.add(this.throwCompleteHandler, this);
	}
    // Second half of snowball slinging, called when first half of animation is complete
	throwCompleteHandler() {
		AudioUtils.play("snowball-throw");
		this.throwing = false;
		var recoil = Dir.getDirectionAsVector(this.direction, false).setMagnitude(-Constants.SLING_RECOIL);
		this.body.velocity.x += recoil.x;
		this.body.velocity.y += recoil.y;
		new Snowball(this);
		this.sprite.animations.play(this.direction + 2, Constants.ANIM_SPEED, false);
		this.lastThrowTime = Date.now();
	}

	// Computes a 0-1 value based on how much the player is spam-throwing
	getJitterFactor() {
		const cooldown = 1000; // TODO: move to Constants
		const timeDelta = Date.now() - this.lastThrowTime;
		// Multiply by 2 because it will usually not be very close to 1.
		return 2 * Math.max(0, (cooldown - timeDelta) / cooldown);
	}

	updateRoll() {
		var slowX = Math.abs(this.body.velocity.x) < Constants.ROLL_SPEED_MIN;
		var slowY = Math.abs(this.body.velocity.y) < Constants.ROLL_SPEED_MIN * Constants.UNIT_ASPECT_RATIO;
		if(slowX) {
			this.body.velocity.x = Math.sign(this.body.velocity.x) * Constants.ROLL_SPEED_MIN;
		}
		if (slowY) {
			this.body.velocity.y = Math.sign(this.body.velocity.y) * Constants.ROLL_SPEED_MIN;
		}
		var rollDir = Dir.getClosestCardinal(this.body.velocity);
		if (this.rollDir !== rollDir) {
			this.rollDir = rollDir;
			this.sprite.play(rollDir + "roll", Constants.ANIM_SPEED / 4, true, false);
		}

		this.sprite.animations.currentAnim.speed = (this.body.speed / 20);
	}

	kill() {
		this.sprite.kill();
	}
}

// Data members
Player.spriteGroup = null;

module.exports = Player;
