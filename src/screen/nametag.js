"use strict";

const TextUtils = require("./textutils");

/*
 * A module for adding nametags to sprites
 */

const NAMETAG_STYLE = {
	font: "32px 'Ravi Prakash'",
	wordWrap: false,
	align: "center",
	fill: "#f9f9eb",
	stroke: "#222222",
	strokeThickness: 3,
	shadow: false,
};

const Nametag = {
	/**
	 * Adds a nametag centered above the given sprite.
	 * @param  {Phaser.sprite} sprite
	 *         The sprite.
	 * @param  {String} name
	 *         The name to display.
	 * @param  {Object} [style=NAMETAG_STYLE]
	 *         (optional) A hash specifying the text style to use. Defaults to NAMETAG_STYLE.
	 */
	addTo: function(sprite, name, style=NAMETAG_STYLE) {
		const game = sprite.game;

		const text = TextUtils.makeText(game, name, style);

		text.anchor.set(0.5, .5);

		// Make shadow follow sprite
		const postUpdate = sprite.postUpdate;
		sprite.postUpdate = function(...args) {
			// TODO: remove ...args?
			postUpdate.call(sprite, ...args);

			text.x = sprite.x + sprite.width / 2;
			text.y = sprite.y;
		};

		// Remove shadow when sprite is removed
		sprite.events.onDestroy.add(function() {
			text.destroy();
		});
	},
};

module.exports = Nametag;
