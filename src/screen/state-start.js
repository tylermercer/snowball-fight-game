"use strict";

const State = require("./basestate");
const Dir = require("../common/utils").Dir;
const TextUtils = require("./textutils");
const AudioUtils = require("./audioutils");

//======================================================================
//			Team class

class Team {
	constructor(/*Phaser.Text*/ namesColumn) {
		this._ids = [];
		this._names = [];
		this._listText = namesColumn;
	}

	addPlayer(id, name) {
		// Only add if it's not already in this team:
		let idx = this._ids.indexOf(id);
		if (idx !== -1) return;

		this._ids.push(id);
		this._names.push(name);
		this.updateList();
	}

	removePlayer(id) {
		// Remove ID
		let idx = this._ids.indexOf(id);
		if (idx !== -1) {
			this._ids.splice(idx, 1);
			this._names.splice(idx, 1);
			this.updateList();
		}
	}

	updateList() {
		this._listText.text = this._names.join("\n");
	}

	get controllers() {
		return this._ids;
	}

	playerCount() {
		return this._ids.length;
	}

	hasPlayer(id) {
		return this._ids.indexOf(id) !== -1;
	}
}

//======================================================================
//			DemoPlayerHelper class

const setRandomTimeout = (fn) => setTimeout(fn, Math.round(600 + Math.random() * 2000));
const getRandomDirection = () => Dir.fromDegrees(Math.random() * 360 - 180);

class DemoPlayerHelper {
	constructor(world) {
		this.world = world;
		this.players = [];
		this.nextID = 1;
	}

	reset() {
		// Clear timeouts for old Players
		for (let data in this.players) {
			clearTimeout(data.timeoutID);
		}
		this.players = [];
		this.nextID = 1;
	}

	add(team) {

		const player = this.world.addPlayer(this.nextID++, "", team);
		const playerData = {};

		const playerAction = function() {
			try {
				player.setDirection(getRandomDirection());
				if (Math.random() < .8) {
					player.slingSnowball();
				}
			} catch (err) {
				// Ingore, but stop updating.
				return;
			}
			// Set a new timeout
			playerData.timeoutID = setRandomTimeout(playerAction);
		};

		player.setDirection(getRandomDirection());
		playerData.timeoutID = setRandomTimeout(playerAction);

		this.players.push(playerData);
	}
}

//======================================================================
//			StartState class

class StartState extends State {

	constructor(game, ac, world) {
		super(game, ac);

		this.sn_world = world;
		this.playerHelper = new DemoPlayerHelper(world);
	}

	init() {
		// Team models:
		this.redTeam = null;
		this.blueTeam = null;
		// List of device IDs:
		this.controllers = [];

		this.playerHelper.reset();
	}

	create() {
		super.create();

		const game = this.game;

		this.sn_world.create();

		const scrim = game.add.graphics(0,0);
		scrim.beginFill(0xFFFFFF, .15);
		scrim.drawRect(0, 0, game.world.width, game.world.height);

		let title = TextUtils.makeText(game, "Snowball Fight!", {fontSize: "128px"});
		title.anchor.set(0.5, 0.5);
		title.x = game.world.centerX;
		title.y = 100;

		let redTeamLabel = TextUtils.makeText(game, "Red Team:", {fontSize: "40px", fill: "#ff4444"});
		redTeamLabel.anchor.set(0.5, 0.5);
		redTeamLabel.x = game.world.centerX * .5;
		redTeamLabel.y = 300;

		let redTeamColumn = TextUtils.makeText(game, "", {fill: "#ff4444"});
		redTeamColumn.anchor.set(0.5, 0);
		redTeamColumn.x = game.world.centerX * .5;
		redTeamColumn.y = 350;

		let blueTeamLabel = TextUtils.makeText(game, "Blue Team:", {fontSize: "40px", fill: "#4444ff"});
		blueTeamLabel.anchor.set(0.5, 0.5);
		blueTeamLabel.x = game.world.centerX * 1.5;
		blueTeamLabel.y = 300;

		let blueTeamColumn = TextUtils.makeText(game, "", {fill: "#4444ff"});
		blueTeamColumn.anchor.set(0.5, 0);
		blueTeamColumn.x = game.world.centerX * 1.5;
		blueTeamColumn.y = 350;

		let message = TextUtils.makeText(game, "", {fontSize: "40px", fill: "#7700aa", wordWrap: true, wordWrapWidth: game.world.width * .8});
		message.anchor.set(0.5, 0.5);
		message.x = game.world.centerX;
		message.y = game.world.height - 100;
		this.message = message;

		// Make text positioning responsive:
		this.scale.onSizeChange.add(function() {
			scrim.width = game.world.width;
			scrim.height = game.world.height;
			title.x = game.world.centerX;
			redTeamLabel.x  = game.world.centerX *  .5;
			redTeamColumn.x  = game.world.centerX *  .5;
			blueTeamLabel.x = game.world.centerX * 1.5;
			blueTeamColumn.x  = game.world.centerX *  1.5;
			message.x = game.world.centerX;
			message.y = game.world.height - 100;
		}, this);

		// Set up team models:
		this.redTeam = new Team(redTeamColumn);
		this.blueTeam = new Team(blueTeamColumn);

		AudioUtils.startBgMusic(game);

		this.playerHelper.add("red");
		this.playerHelper.add("blue");
	}

	update() {
		this.sn_world.update();
	}

	onAcReady() {
		this.controllers = this.ac.getControllerDeviceIds();
		super.onAcReady();
		this.updateTeamsReadyState();
	}

	onConnect(device_id) {
		// Add ID to list
		this.controllers.push(device_id);
		this.updateTeamsReadyState();
	}

	onDisconnect(device_id) {
		// Remove ID from list and teams
		let idx = this.controllers.indexOf(device_id);
		if (idx !== -1) this.controllers.splice(idx, 1);

		this.blueTeam.removePlayer(device_id);
		this.redTeam.removePlayer(device_id);
		this.updateTeamsReadyState();
	}

	onMessage(device_id, msg) {
		if (msg.team) {
			let name = this.ac.getNickname(device_id);
			if (msg.team === "red") { // Red team
				this.blueTeam.removePlayer(device_id);
				this.redTeam.addPlayer(device_id, name);
			} else { // Blue team
				this.redTeam.removePlayer(device_id);
				this.blueTeam.addPlayer(device_id, name);
			}
			this.updateTeamsReadyState();

		} else if (msg.requestStartGame && !this.startingGame && this.updateTeamsReadyState()) {

			this.startingGame = true;
			// Start game after 5 second countdown
			countDownFrom(5, secondsLeft => { // Count:
				if (this.updateTeamsReadyState()) {
					this.message.text = "Starting game in " + secondsLeft + "...";
				} else {
					// Not ready anymore, cancel the countdown:
					this.startingGame = false;
					return false;
				}
			}, () => { // Done:
				this.message.text = "";
				// Kill the demo players' timeout loops:
				this.playerHelper.reset();
				// Start the game!
				let teams = {
					"red": this.redTeam.controllers,
					"blue": this.blueTeam.controllers,
				};
				// See post here: https://goo.gl/I6u1cp
				this.game.state.start("play", true, false, {
					teams: teams,
					sitOuts: this.sitOuts,
				});
				this.startingGame = false;
			});

		}
	}

	updateTeamsReadyState() {
		let redCount = this.redTeam.playerCount();
		let blueCount = this.blueTeam.playerCount();
		let masterID = this.ac.getMasterControllerDeviceId();

		let ready;
		if ((redCount + blueCount) < 2) {
			ready = false;
			this.message.text = "At least two players must join!";
		} else if (Math.abs(redCount - blueCount) > 1) {
			ready = false;
			this.message.text = "The teams need to be balanced!";
		} else {
			ready = true;

			// Process sit-outs
			this.sitOuts = this.controllers.filter(id =>
				!this.redTeam.hasPlayer(id) && !this.blueTeam.hasPlayer(id)
			);
			const sitOutCount = this.sitOuts.length;
			if (sitOutCount) {
				const sitOutNames = this.sitOuts.map(id =>
					this.ac.getNickname(id)
				);
				// Make a grammatically correct message:
				if (sitOutCount === 1) {
					this.message.text = sitOutNames[0]+" hasn't joined.";
				} else if (sitOutCount === 2) {
					this.message.text = sitOutNames[0]+" and "+sitOutNames[1]+" haven't joined.";
				} else {
					this.message.text =
						sitOutNames.slice(0,-1).join(", ") +
						", and " + sitOutNames.slice(-1)[0] +
						" haven't joined.";
				}
				this.message.text += " Join a team to play!\n";
			} else {
				this.message.text = "";
			}

			let masterName = this.ac.getNickname(masterID);
			this.message.text += "Waiting for " + masterName + " to start the game...";
		}

		// Send ready state to first player
		this.ac.message(masterID, {allowStartGame: ready});
		return ready;
	}

	/* Called by Phaser when leaving this state */
	shutdown() {
		this.scale.onSizeChange.removeAll(this);
	}
}

module.exports = StartState;

/**
 * A utility function for doing a countdown.
 * @param  {Number}   seconds The number of seconds to count down.
 * @param  {Function} count   A function to call every second. Given the
 *                            number of seconds left as a parameter. May
 *                            return false to cancel the countdown.
 * @param  {Function} done    A function to call at the end of the countdown.
 */
function countDownFrom(seconds, count, done) {
	let counter = seconds;
	function loop() {
		if (counter) {
			if (count(counter) === false) return;
			counter--;
			setTimeout(loop, 1000);
		} else {
			done();
		}
	}
	loop();
}
