/*
 * Apparently Phaser 2 is NOT well-suited for bundling with webpack. So these
 * haxx fix it. See https://www.npmjs.com/package/phaser-ce#browserify--commonjs
 */

window.PIXI = require("phaser-ce/build/custom/pixi.min.js");
module.exports = window.Phaser = require("phaser-ce/build/custom/phaser-arcade-physics.min.js");
