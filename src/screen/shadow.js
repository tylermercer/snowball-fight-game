"use strict";

const SHADOW_COLOR = 0x000000;
const SHADOW_ALPHA = .2;

const Shadows = {};

/**
 * Call this in the create() phase. Sets up a group for the drop shadows for
 * proper z-ordering.
 * @param  {Phaser.Game} game The game
 */
Shadows.init = function(game) {
	// Create a new group named "shadows", and cache it as sn_shadows
	game.sn_shadows = game.add.group(undefined, "shadows");
};

/**
 * Adds a drop shadow to the given sprite. The shadow is positioned based off of
 * the sprite's collision body.
 * @param {Phaser.Sprite} sprite The Sprite to add a shadow to.
 */
Shadows.addFor = function(sprite) {
	const body = sprite.body;
	const game = sprite.game;

	const shadow = game.add.graphics(0, 0);
	shadow.beginFill(SHADOW_COLOR, SHADOW_ALPHA);
	// Draw an ellipse with the same width and height as the bounding box.
	shadow.drawEllipse(0, 0, body.width/2, body.height/2);
	shadow.sn_elevation = 0;

	// Add to group
	game.sn_shadows.add(shadow);

	// Make shadow follow sprite
	const postUpdate = sprite.postUpdate;
	sprite.postUpdate = function(...args) {
		// TODO: remove ...args?
		postUpdate.call(sprite, ...args);

		shadow.x = sprite.x + body.offset.x + body.width / 2;
		shadow.y = sprite.y + body.offset.y + body.height / 2 - shadow.sn_elevation;
	};

	// Remove shadow when sprite is removed
	sprite.events.onDestroy.add(function() {
		shadow.destroy();
	});

	// Cache for use by other functions
	sprite.sn_shadow = shadow;
};

Shadows.updateFor = function(sprite) {
	const body = sprite.body;
	const shadow = sprite.sn_shadow;

	shadow.width = body.width;
	shadow.height = body.height;
};

module.exports = Shadows;
