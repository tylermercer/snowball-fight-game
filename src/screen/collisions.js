"use strict";

const TextUtils = require("./textutils");
const AudioUtils = require("./audioutils");

/**
* The module for handling collisions
*/

class CollisionsHandler {

	constructor(players, game) {
		// The map of device IDs to Player objects.
		this.players = players;
		this.game = game;
	}

	collide(spriteGroup) {
		this.game.physics.arcade.collide(spriteGroup, undefined,
			null, this.handle, this);
	}

	handle(/*Sprite*/ sprite1, /*Sprite*/ sprite2) {
		const players = this.players;
		if (sprite1.sn_snowball) {
			const snowball = sprite1.sn_snowball;

			if(sprite2.sn_player_id) {
				// 1 is snowball, 2 is player
				return this.playerWithSnowball(sprite2, sprite1);

			} else if (!sprite2.sn_snowball) {
				// 1 is snowball, 2 is not snowball or player
				snowball.elevateShadow();
			}
			return false; // Don't do physics.

		} else if (sprite2.sn_snowball) {
			// Call again in other order, to keep code DRY.
			return this.handle(sprite2, sprite1);

		} else if (sprite1.sn_player_id && sprite2.sn_player_id) {
			// Both are players
			return this.playerWithPlayer(players[sprite1.sn_player_id], players[sprite2.sn_player_id]);

		} else {
			//Collide as normal
			return true;
		}
	}

	//======================================================================
	//			Player + Player

	playerWithPlayer(/*Player*/ p1, /*Player*/ p2) {
		// Check if one is defeated and the other is not
		if (p1.isDefeated === p2.isDefeated) {
			// Both have same state, collide as normal
			return true;

		} else if (p1.isDefeated && !p2.isDefeated) {
			// The first is defeated, the second is not -> flatten the second
			p2.flatten(p1.rollDir);
			// No collisions
			return false;

		} else {
			// The second is defeated, the first is not -> flatten the first
			p1.flatten(p2.rollDir);
			// No collisions
			return false;
		}
	}

	//======================================================================
	//			Player + Snowball

	playerWithSnowball(/*Sprite*/ playerSprite, /*Sprite*/ snowballSprite) {
		const players = this.players;
		const player = players[playerSprite.sn_player_id];
		const snowball = snowballSprite.sn_snowball;

		if (player !== snowballSprite.sn_source && !(player.isKnockedDown)) {
			const wasDefeated = player.isDefeated;
			// The player got hit!
			player.snowballImpact(snowball.direction, snowballSprite.body.velocity);
			if (!wasDefeated) {
				this.onPlayerHit(player);
			}
			// Play sound effect
			AudioUtils.play("snowball-hit");
			// Show "Hit!" text
			this.showHitText(snowballSprite.position);
			// Remove the snowball.
			snowball.kill();
		}
		// Never do collisions:
		return false;
	}

	showHitText(position) {
		const style = {
			fontSize: "24px",
			fill: "#ff1111",
			stroke: "#880000",
			strokeThickness: 1,
		};
		let txt = TextUtils.makeText(this.game, "Hit!", style);
		txt.position.copyFrom(position);
		txt.anchor.set(0.5, 0);

		// All these are in millis:
		const duration = 800;
		const step = 30;
		let progress = 0;
		const from = {
			alpha: 1,
			y: position.y,
		};
		const to = {
			alpha: 0,
			y: position.y - 30,
		};
		// easing functions:
		const easing = {
			alpha: x => x*x,
			y: x => x,
		};

		// Hand-written tween (so it doesn't get stopped by a state change).
		const intervalID = setInterval(() => {
			progress += step;
			let frac = progress / duration;

			for (let attrib in from) {
				txt[attrib] =
						from[attrib] +
						easing[attrib](frac) * (to[attrib] - from[attrib]);
			}
		}, step);

		// End of tween: stop interval and destroy text
		setTimeout(() => {
			clearInterval(intervalID);
			txt.destroy();
		}, duration);
	}

	//======================================================================
	//			Event listeners (abstract)

	onPlayerHit() {}

}

module.exports = CollisionsHandler;
