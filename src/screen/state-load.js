"use strict";

const State = require("./basestate");
const fillPhaser = require("../fillPhaser");
const TextUtils = require("./textutils");
const AudioUtils = require("./audioutils");

const TEXT_STYLE = {
	font: "32px 'Ravi Prakash'",
	wordWrap: false,
	align: "center",
	fill: "#f9f9eb",
	stroke: "#222222",
	strokeThickness: 3,
	shadow: false,
};

class LoadState extends State {

	preload() {

		fillPhaser.makePhaserFillScreen(this.game);

		// Create "Loading" text
		const game = this.game;
		let txt = TextUtils.makeText(game, "Loading...", TEXT_STYLE);
		txt.anchor.set(0.5, 0.5);
		txt.x = game.world.centerX;
		txt.y = game.world.centerY;

		this.scale.onSizeChange.add(function() {
			txt.x = game.world.centerX;
			txt.y = game.world.centerY;
		});
		// Prevent stopping on focus lost:
		game.stage.disableVisibilityChange = true;

		game.load.spritesheet("billy_0_red", "sprites/player/billy_0_red.png", 128, 128);
		game.load.spritesheet("billy_0_blue", "sprites/player/billy_0_blue.png", 128, 128);
		game.load.spritesheet("billy_1_red", "sprites/player/billy_1_red.png", 128, 128);
		game.load.spritesheet("billy_1_blue", "sprites/player/billy_1_blue.png", 128, 128);
		game.load.spritesheet("billy_2_red", "sprites/player/billy_2_red.png", 128, 128);
		game.load.spritesheet("billy_2_blue", "sprites/player/billy_2_blue.png", 128, 128);
		game.load.spritesheet("billy_3_red", "sprites/player/billy_3_red.png", 128, 128);
		game.load.spritesheet("billy_3_blue", "sprites/player/billy_3_blue.png", 128, 128);
		game.load.spritesheet("bullet", "sprites/snowball.png", 16, 16);
		game.load.image("ice_bg", "sprites/map/bg_ice3.png");
		game.load.image("wall_north", "sprites/map/wall_north.png");
		game.load.image("wall_south", "sprites/map/wall_south.png");
		game.load.image("wall_west", "sprites/map/wall_west.png");
		game.load.image("wall_northwest", "sprites/map/wall_northwest.png");
		game.load.image("wall_northeast", "sprites/map/wall_northeast.png");
		game.load.image("wall_southwest", "sprites/map/wall_southwest.png");
		game.load.image("wall_southeast", "sprites/map/wall_southeast.png");
		game.load.image("wall_outer_collider", "sprites/map/wall_outer_collider.png");

		AudioUtils.preload(game);
	}

	create() {
		this.game.state.start("start");
	}
}

module.exports = LoadState;
