"use strict";

const State = require("./basestate");
const AudioUtils = require("./audioutils");

class PlayState extends State {

	constructor(game, ac, world) {
		super(game, ac);
		this.sn_world = world;
	}

	init(params) {
		// A map of team colors to arrays of team member device ids.
		this.teams = params.teams;
		// A list of devices that chose to "sit out" this round
		this.sitOuts = params.sitOuts;
		// Sprite group for the player sprites, snowball sprites, and obstacle sprites
		this.sprites = null;
		// Array of all controller device_id's
		this.controllers = [];
		// A map containing `[device_id]: true` for each disconnected device
		this.disconnectedControllers = {};

		this.pausedControllers = [];
	}

	getPlayerTeam(id) {
		let smallestTeam = null;
		let smallestTeamPlayerCount = Number.POSITIVE_INFINITY;

		for (let teamName in this.teams) {
			let teamMembers = this.teams[teamName];
			// Look for team already containing that player
			if (teamMembers.includes(id)) {
				return teamName;
			}
			// Look for team with smallest number of players
			if (teamMembers.length < smallestTeamPlayerCount) {
				smallestTeam = teamName;
				smallestTeamPlayerCount = teamMembers.length;
			}
		}
		// No team assigned to that player yet, so add it to the one with the
		// least players:
		this.teams[smallestTeam].push(id);
		return smallestTeam;
	}

	//======================================================================
	//			CREATE

	create() {
		super.create();
		const game = this.game;

		this.sn_world.create();
		this.sn_world.addWalls();

		this.sn_world.onPlayerHit =
				(player) => this.onPlayerHit(player);

		AudioUtils.startGame(game);
	}

	onAcReady() {
		// Call the listener manually for all the devices that are already connected:
		this.ac.getControllerDeviceIds().forEach(id => this.onConnect(id));
		super.onAcReady();
	}

	// As soon as a device connects we add it to our device-map
	onConnect(/*Number*/ device_id) {
		const airconsole = this.ac;

		// Ignore if it's in the sitOuts list
		if (this.sitOuts.indexOf(device_id) !== -1) {
			airconsole.message(device_id, {state: "waiting"});
			return;
		}

		const controllers = this.controllers;
		const disconnectedControllers = this.disconnectedControllers;

		controllers.push(device_id);
		// instantiate player
		const playerName = airconsole.getNickname(device_id);

		let team = this.getPlayerTeam(device_id);

		// Make sure there is not already a Player created for this device
		if (!disconnectedControllers[device_id]) {

			this.sn_world.addPlayer(device_id, playerName, team);
			// (world.addPlayer positions the player for us.)
		}
		airconsole.message(device_id, {startGame: team});
	}

	// Called when a device disconnects (can take up to 5 seconds after device left)
	onDisconnect(/*Number*/ device_id) {

		// If it's a sit-out player, remove it from that list but do nothing else,
		// so it looks like a fresh connect if they connect again:
		const soIndex = this.sitOuts.indexOf(device_id);
		if (soIndex !== -1) {
			this.sitOuts.splice(soIndex, 1);
			return;
		}

		const controllers = this.controllers;
		const disconnectedControllers = this.disconnectedControllers;
		// Mark the controller as disconnected
		disconnectedControllers[device_id] = true;
		// Remove the controller from the list
		var index = controllers.indexOf(device_id);
		if (index !== -1) {
			controllers.splice(index, 1);
		}

		// Send an unpause request from this player
		this.updatePausedState(device_id, false);
	}

	onMessage(/*Number*/ device_id, /*object*/ message) {
		const player = this.sn_world.getPlayerForID(device_id);

		if (message.dir) player.setDirection(message.dir);
		if (message.fire) player.slingSnowball();
		if (message.pause !== undefined) this.updatePausedState(device_id, !!message.pause);
	}

	/**
	 * Called by airconsole.onMessage when a player taps the pause or resume button.
	 * @param  {String}  device_id      The ID of the requesting device.
	 * @param  {Boolean} pauseRequested True if they tapped pause btn, false if they tapped resume btn.
	 */
	updatePausedState(device_id, pauseRequested) {
	// Delete device_id from array:
		let idx = this.pausedControllers.indexOf(device_id);
		if (idx !== -1) {
			// They are in the array; only remove if an UNpause was requested.
			if (!pauseRequested) this.pausedControllers.splice(idx, 1);

		} else if (pauseRequested) {
			// They aren't in the array; only add if a pause was requested.
			this.pausedControllers.push(device_id);
		}

	// Update paused state
		this.game.paused = !!this.pausedControllers.length;
	}

	//======================================================================
	//			UPDATE

	update() {
		this.sn_world.update();
	}

	//======================================================================
	//			Game events

	/**
	 * Called by CollisionsHandler when a non-defeated Player gets hit
	 * @param  {Player} player The Player that was hit
	 */
	onPlayerHit(player) {
		// Tell controller to vibrate:
		this.ac.message(player.id, {vib:1});

		// Check if the player just got defeated
		if (player.isDefeated) {
			// Check if the player's team is all defeated
			if (this.checkIfTeamLost(player.team)) {
				this.onTeamLost(player.team);
			} else {
				// Player still has non-defeated teammates, so tell controller to go to defeated state.
				this.ac.message(player.id, {state:"defeated"});
			}
		}
	}

	checkIfTeamLost(teamName) {
		const teamMembers = this.teams[teamName];

		for (let idx in teamMembers) {
			// Get the Player:
			const player = this.sn_world.getPlayerForID(teamMembers[idx]);
			// Team isn't defeated if the Player can't be found or isn't defeated
			if (!player || !player.isDefeated) {
				return false;
			}
		}
		return true;
	}

	onTeamLost(losingTeam) {
		// Determine winning team
		let winningTeam = "";
		for (let team in this.teams) {
			if (team !== losingTeam) {
				winningTeam = team;
				break;
			}
		}

		// Start EndState
		const params = {
			restartParams: {
				teams: this.teams,
				sitOuts: this.sitOuts,
			},
			winningTeam: winningTeam,
		};
		this.game.state.start("end", false, false, params);
	}
}

module.exports = PlayState;
