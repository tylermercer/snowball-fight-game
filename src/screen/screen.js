"use strict";

/* AirConsole API must be loaded before this script */
/* global AirConsole */

const Phaser = require("phaser");

const game = new Phaser.Game(800, 600, Phaser.AUTO, "SnowbillFightAC");
window.game = game;
const airconsole = new AirConsole;
window.airconsole = airconsole;


//======================================================================
//			souped-up AirConsole ready listener

let ready = false;
let code = "";
let todo = null;
airconsole.doWhenReady = function(func) {
	if (ready) {
		// If it's already ready, call the function right away
		setTimeout(() => func(code));
	} else {
		// Otherwise, wait for AC to be ready
		todo = func;
	}
};
airconsole.onReady = function(acCode) {
	code = acCode;
	ready = true;
	if (todo) {
		todo(acCode);
	}
};

airconsole.onAdShow = function() {
	console.log("Showing ad");
	game.sound.mute = true;
};
airconsole.onAdComplete = function() {
	console.log("Ad complete");
	game.sound.mute = false;
};

//======================================================================
//			Phaser fill-screen setup

require("../fillPhaser").init(game);

//======================================================================
//			States

const World = require("./world");
const LoadState = require("./state-load");
const StartState = require("./state-start");
const PlayState = require("./state-play");
const EndState = require("./state-end");

const world = new World(game, airconsole);

game.state.add("load", new LoadState(game));
game.state.add("start", new StartState(game, airconsole, world));
game.state.add("play", new PlayState(game, airconsole, world));
game.state.add("end", new EndState(game, airconsole, world));

game.state.start("load");
