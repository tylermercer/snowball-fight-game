"use strict";

/*
 * Contains utilities for dealing with sound effects
 */

const audio = {};

const addAudio = function(game, name) {
	// If the audio hasn't been added yet, add it:
	if (!audio[name]) {
		let sound = game.add.audio(name);
		audio[name] = sound;
	}
	return audio[name];
};

const AudioUtils = {
	/**
	 * Call this in the preload() of your game.
	 * @param  {Phaser.Game} game The Phaser game
	 */
	preload: function(game) {
		game.load.audio("bgmusic", "audio/music.ogg");
		game.load.audio("snowball-hit", "audio/hit05.mp3");
		game.load.audio("snowball-throw", "audio/throw.ogg");
		game.load.audio("game-start", "audio/game_start.ogg");
	},

	/**
	 * Starts the background music. Call this at the start screen.
	 * @type {Phaser.Game} game The Phaser game
	 */
	startBgMusic: function(game) {
		let bg = addAudio(game, "bgmusic");
		bg.loop = true;
		bg.volume = .3;
		if (!bg.isPlaying) bg.play();
	},

	/**
	 * Call this in the create() of the play state.
	 * @param  {Phaser.Game} game The Phaser game
	 */
	startGame: function(game) {
		addAudio(game, "snowball-hit").allowMultiple = true;
		addAudio(game, "snowball-throw").allowMultiple = true;
		addAudio(game, "game-start").play();
	},

	/**
	 * @param  {String} sound The name of the sound effect to play
	 */
	play: function(sound) {
		if (audio[sound]) {
			audio[sound].play();
		}
	},
};

module.exports = AudioUtils;
