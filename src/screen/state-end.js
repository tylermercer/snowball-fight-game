"use strict";

const State = require("./basestate");
const TextUtils = require("./textutils");
const Constants = require("../common/utils").Constants;

class EndState extends State {

	constructor(game, ac, world) {
		super(game, ac);

		this.sn_world = world;
		this.numOfGamesCompleted = 0;
	}

	init(params) {
		this.restartParams = params.restartParams;
		this.winningTeam = params.winningTeam;

		this.controllers = [];

		// Tracks which controllers are ready to play again
		this.againReadyList = [];
		// Whether to change teams (i.e. return to start screen) when beginning a new game
		this.againChangeTeams = true;
	}

	//======================================================================
	//			CREATE

	create() {
		super.create();

		const game = this.game;
		const winningTeam = this.winningTeam;

		// Announce winning team
		const style = {
			fill: this.winningTeam === "red" ? "#ff4444" : "#4444ff",
			fontSize: "64px",
		};
		const txt = TextUtils.makeText(game, winningTeam.toUpperCase() + " TEAM WINS!", style);
		txt.anchor.set(0.5, 0.5);
		txt.x = game.world.width/2;
		txt.y = game.world.height/2;

		// Every 3 games, show an ad
		this.numOfGamesCompleted = (this.numOfGamesCompleted + 1) % Constants.GAMES_PER_AD;
		if (this.numOfGamesCompleted === 0) {
			setTimeout(() => this.ac.showAd(), 1000);
		}

		// Reset player hit listener:
		this.sn_world.onPlayerHit = () => {};
	}

	onAcReady() {
		// Call the listener manually for all the devices that are already connected:
		this.ac.getControllerDeviceIds().forEach(id => this.onConnect(id));
		super.onAcReady();
	}

	// As soon as a device connects we add it to our device-map
	onConnect(/*Number*/ device_id) {
		this.controllers.push(device_id);

		if (this.sn_world.getPlayerForID(device_id)) { // Device is an existing player

			// Tell controller to go to Game Over state
			let masterPlayer = this.ac.getMasterControllerDeviceId();
			this.ac.message(device_id, {
				gameover: device_id === masterPlayer,
			});

		} else { // Device is a new player joining after game ended

			// Record that controller as being "ready"
			this.againReadyList.push(device_id);

			// Tell controller to go to waiting state
			this.ac.message(device_id, {state: "waiting"});

		}
	}

	// Called when a device disconnects (can take up to 5 seconds after device left)
	onDisconnect(/*Number*/ device_id) {
		// Remove the device from the array of devices
		var index = this.controllers.indexOf(device_id);
		if (index !== -1) {
			this.controllers.splice(index, 1);
		}
		// Remove the device from the "ready" list if it's in there.
		var arlIndex = this.againReadyList.indexOf(device_id);
		if (arlIndex !== -1) {
			this.againReadyList.splice(arlIndex, 1);
		}
	}

	onMessage(/*Number*/ device_id, /*object*/ message) {
		if (message.again) {
			this.onReadyMessage(device_id, message.again);
		}
	}

	//======================================================================
	//			UPDATE

	update() {
		this.sn_world.update();
	}

	onReadyMessage(id, againMsg) {
		if (!this.againReadyList.includes(id)) {
			// Track which players are ready:
			this.againReadyList.push(id);
			// Track wether to keep same teams ("same") or change teams ("change"):
			if (againMsg === "same") this.againChangeTeams = false;
		}
		// Check if all players are ready
		if (this.againReadyList.length === this.controllers.length) {
			// Update game state
			if (this.againChangeTeams) {
				this.backToStart();
			} else {
				this.restart();
			}
		} else {
			// Tell controller to go to waiting state
			this.ac.message(id, {state: "waiting"});
		}
	}

	/* Restarts the Play state with the same team parameters */
	restart() {
		// See post here: https://goo.gl/I6u1cp
		this.game.state.start("play", true, false, this.restartParams);
	}

	/* Returns to the start screen */
	backToStart() {
		this.ac.broadcast({state: "team-select"});
		this.game.state.start("start");
	}
}

module.exports = EndState;
