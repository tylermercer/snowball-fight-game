"use strict";

const Phaser = require("phaser");

const WALL_SPRITE_WIDTH = 64;
const WALL_BODY_WIDTH = 46;
const WALL_BODY_WIDTH_S = 18;
const WALL_BODY_WIDTH_N = 43;
const FOREGROUND_WEIGHT = 200;
const MIN_MARGIN_HORIZONTAL = 64;

const MapManager = {};
var game = null;

/**
 * Call this in the create() phase. Creates the tiling background and initializes the background color
 * @param {Phaser.Game} game The game
 */
MapManager.init = function(/*Phaser.Game*/ _game) {
	game = _game;
	// Set up the background
	game.stage.backgroundColor = "#FFFFFF";
	MapManager.bg = MapManager.makeBackgroundSprite();
	this.hasWalls = false;
};

MapManager.makeBackgroundSprite = function() {
	// Make a background sprite that fills the screen:
	return game.add.tileSprite(0, 0, game.world.width, game.world.height, "ice_bg");
};

/**
 * Call this after setting MapManager.spriteGroup. Creates the walls and configures them for proper physics.
 */
MapManager.buildWalls = function() {
	this.hasWalls = true;
	this.dimens = MapManager.getArenaDimensions();
	let dimens = this.dimens;

	// Resize background to proper size:
	this.bg.x = dimens.offsetX;
	this.bg.y = dimens.offsetY;
	this.bg.width = dimens.x;
	this.bg.height = dimens.y;

	//Build North wall
	let wall_north = game.add.tileSprite(dimens.offsetX + WALL_SPRITE_WIDTH, dimens.offsetY, dimens.x - WALL_SPRITE_WIDTH * 2, WALL_SPRITE_WIDTH, "wall_north");
	game.physics.enable(wall_north, Phaser.Physics.ARCADE);
	wall_north.body.immovable = true;
	wall_north.body.allowGravity = false;
	wall_north.body.width = dimens.x;
	wall_north.body.offset.x = -WALL_SPRITE_WIDTH;
	wall_north.body.height = WALL_BODY_WIDTH_N;
	wall_north.sn_foreground = 0;
	MapManager.spriteGroup.add(wall_north);

	//Build West wall
	let wall_west = game.add.tileSprite(dimens.offsetX, dimens.offsetY + WALL_SPRITE_WIDTH, WALL_SPRITE_WIDTH, dimens.y - WALL_SPRITE_WIDTH, "wall_west");
	game.physics.enable(wall_west, Phaser.Physics.ARCADE);
	wall_west.body.immovable = true;
	wall_west.body.allowGravity = false;
	wall_west.body.height = dimens.y - WALL_BODY_WIDTH_N - WALL_BODY_WIDTH_S;
	wall_west.body.offset.y = WALL_BODY_WIDTH - WALL_SPRITE_WIDTH;
	wall_west.body.width = WALL_BODY_WIDTH;
	wall_west.sn_foreground = 0;
	MapManager.spriteGroup.add(wall_west);

	//Build East wall
	let wall_east = game.add.tileSprite(dimens.offsetX + dimens.x, dimens.offsetY + WALL_SPRITE_WIDTH, WALL_SPRITE_WIDTH, dimens.y - WALL_SPRITE_WIDTH, "wall_west");
	game.physics.enable(wall_east, Phaser.Physics.ARCADE);
	// wall_east.anchor.setTo(0.5, 1);
	wall_east.scale.x = -1;
	wall_east.body.immovable = true;
	wall_east.body.allowGravity = false;
	wall_east.body.height = dimens.y - WALL_BODY_WIDTH_N - WALL_BODY_WIDTH_S;
	wall_east.body.offset.y = WALL_BODY_WIDTH - WALL_SPRITE_WIDTH;
	// wall_east.body.offset.x = WALL_SPRITE_WIDTH - WALL_BODY_WIDTH;
	wall_east.body.width = WALL_BODY_WIDTH;
	wall_east.sn_foreground = 0;
	MapManager.spriteGroup.add(wall_east);

	//Build South wall
	let wall_south = game.add.tileSprite(dimens.offsetX + WALL_SPRITE_WIDTH, dimens.offsetY + dimens.y - WALL_SPRITE_WIDTH, dimens.x - WALL_SPRITE_WIDTH * 2, WALL_SPRITE_WIDTH, "wall_south");
	game.physics.enable(wall_south, Phaser.Physics.ARCADE);
	wall_south.body.immovable = true;
	wall_south.body.allowGravity = false;
	wall_south.body.width = dimens.x;
	wall_south.body.offset.x = -WALL_SPRITE_WIDTH;
	wall_south.body.offset.y = WALL_SPRITE_WIDTH - WALL_BODY_WIDTH_S;
	wall_south.body.height = WALL_BODY_WIDTH_S;
	wall_south.sn_foreground = FOREGROUND_WEIGHT;
	MapManager.spriteGroup.add(wall_south);

	//Build corners
	MapManager.spriteGroup.add(game.add.image(dimens.offsetX, dimens.offsetY, "wall_northwest")); //nw

	MapManager.spriteGroup.add(game.add.image(dimens.offsetX + dimens.x - WALL_SPRITE_WIDTH, dimens.offsetY, "wall_northeast")); //ne

	let corner_southeast = game.add.image(dimens.offsetX + dimens.x - WALL_SPRITE_WIDTH, dimens.offsetY + dimens.y - WALL_SPRITE_WIDTH, "wall_southeast"); //se
	corner_southeast.sn_foreground = FOREGROUND_WEIGHT;
	MapManager.spriteGroup.add(corner_southeast);

	let corner_southwest = game.add.image(dimens.offsetX, dimens.offsetY + dimens.y - WALL_SPRITE_WIDTH, "wall_southwest"); //sw
	corner_southwest.sn_foreground = FOREGROUND_WEIGHT;
	MapManager.spriteGroup.add(corner_southwest);

	//Build outer colliders
	let outer_collider_n = game.add.sprite(0, 0, "wall_outer_collider");
	game.physics.enable(outer_collider_n, Phaser.Physics.ARCADE);
	outer_collider_n.body.immovable = true;
	outer_collider_n.body.allowGravity = false;
	outer_collider_n.body.width = dimens.x;
	outer_collider_n.body.offset.x = dimens.offsetX;
	outer_collider_n.body.height = WALL_BODY_WIDTH_N/2;
	MapManager.spriteGroup.add(outer_collider_n);

	let outer_collider_s = game.add.sprite(0, 0, "wall_outer_collider");
	game.physics.enable(outer_collider_s, Phaser.Physics.ARCADE);
	outer_collider_s.body.immovable = true;
	outer_collider_s.body.allowGravity = false;
	outer_collider_s.body.width = dimens.x;
	outer_collider_s.body.offset.x = dimens.offsetX;
	outer_collider_s.body.offset.y = dimens.y + dimens.offsetY - WALL_BODY_WIDTH_S/2;
	outer_collider_s.body.height = WALL_BODY_WIDTH_S/2;
	MapManager.spriteGroup.add(outer_collider_s);

	let outer_collider_e = game.add.sprite(0, 0, "wall_outer_collider");
	game.physics.enable(outer_collider_e, Phaser.Physics.ARCADE);
	outer_collider_e.body.immovable = true;
	outer_collider_e.body.allowGravity = false;
	outer_collider_e.body.height = dimens.y;
	outer_collider_e.body.offset.y = dimens.offsetY;
	outer_collider_e.body.offset.x = dimens.x + dimens.offsetX - WALL_BODY_WIDTH/2;
	outer_collider_e.body.width = WALL_BODY_WIDTH/2;
	MapManager.spriteGroup.add(outer_collider_e);

	let outer_collider_w = game.add.sprite(0, 0, "wall_outer_collider");
	game.physics.enable(outer_collider_w, Phaser.Physics.ARCADE);
	outer_collider_w.body.immovable = true;
	outer_collider_w.body.allowGravity = false;
	outer_collider_w.body.height = dimens.y;
	outer_collider_w.body.offset.y = dimens.offsetY;
	outer_collider_w.body.offset.x = dimens.offsetX;
	outer_collider_w.body.width = WALL_BODY_WIDTH/2;
	MapManager.spriteGroup.add(outer_collider_w);
};

MapManager.update = function() {
	if (!this.hasWalls) {
		this.bg.width = game.world.width;
		this.bg.height = game.world.height;
	}
};

MapManager.getPointInArena = function(bufferX, bufferY) {
	return {
		x: game.rnd.integerInRange(this.dimens.offsetX + WALL_BODY_WIDTH*2, this.dimens.offsetX + this.dimens.x - WALL_BODY_WIDTH*2 - bufferX),
		y: game.rnd.integerInRange(this.dimens.offsetY + WALL_BODY_WIDTH_N*2, this.dimens.offsetY + this.dimens.y - WALL_BODY_WIDTH_S*2 - bufferY),
	};
};


MapManager.getArenaDimensions = function() {
	let width = (game.world.width - MIN_MARGIN_HORIZONTAL*2 - WALL_SPRITE_WIDTH*2) - (game.world.width - MIN_MARGIN_HORIZONTAL*2 - WALL_SPRITE_WIDTH*2) % 256;
	let height = game.world.height;
	width += WALL_SPRITE_WIDTH *2;
	return {x: width, y: height, offsetX: Math.round((game.world.width - width)/2), offsetY: 0};
};

MapManager.spriteGroup = null;

module.exports = MapManager;
