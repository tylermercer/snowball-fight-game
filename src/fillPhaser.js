"use strict";

const Phaser = require("phaser");

function init(/*Phaser.Game*/ game) {
	window.addEventListener("resize", resizeCanvas);
	window.addEventListener("fullscreenchange", resizeCanvas);

	function resizeCanvas() {
		game.scale.setGameSize(window.innerWidth, window.innerHeight);
		var canvas  = document.getElementById("SnowbillFightAC").firstChild;
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
}

// Call in preload()
function makePhaserFillScreen(/*Phaser.Game*/ game) {
	game.scale.scaleMode = Phaser.ScaleManager.RESIZE;
	game.stage.disableVisibilityChange = true;
}

module.exports = {
	init: init,
	makePhaserFillScreen: makePhaserFillScreen,
};
