"use strict";

/* global AirConsole */
const Dir = require("../common/utils").Dir;

//======================================================================
//			Init AirConsole

const airconsole = new AirConsole({
	orientation: AirConsole.ORIENTATION_LANDSCAPE,
});

airconsole.onReady = onReady;

//======================================================================
//			Set up Event listeners

function onReady() {

	setupStateChange();

	// Set up team select event listeners
	setupTeamSelect();

	// Set up controller event listeners
	setupDPad();
	setupFireButton();

	// Defeated state:
	setupGetHeroBtn();

	// Game Over state:
	setupGameover();

	airconsole.onMessage = onAcMessage;
	airconsole.onPremium = id => (id === airconsole.getDeviceId()) && onAcPremium();
}

/**
 * Called when this AirConsole controller recieves a message.
 * @param  {Number} device_id The ID of the sender.
 * @param  {Object} msg       The message
 */
function onAcMessage(unused_device_id, msg) {
	// Vibrate
	if (msg.vib) airconsole.vibrate(100);
	// Enable "Start game" button
	if (msg.allowStartGame !== undefined) setStartGameButtonEnabled(msg.allowStartGame);
	// Start game
	if (msg.startGame) {
		// If it's a string, it's a team assignment
		if (typeof msg.startGame === "string") {
			const teamName = msg.startGame;
			document.getElementById("team-" + teamName).checked = true;
			updateBackgroundColor(teamName);
		}
		startGame();
	}
	// End of game
	if (msg.gameover !== undefined) {
		showGameOver(msg.gameover/*true if player is the master player, false otherwise*/);
	}
	if (msg.state) setState(msg.state);
}

//======================================================================
//			State tracking

function setupStateChange() {
	var els = document.querySelectorAll("input[name='state']");
	for (let i = 0; i < els.length; i++) {
		els[i].onchange = onStateChanged;
	}
}

function onStateChanged() {
	switch (this.id) {
		case "state-paused":
			airconsole.message(0, {pause: 1});
			break;
		case "state-controller":
			airconsole.message(0, {pause: 0});
			break;
	}
}

function setState(/*String*/ state) {
	document.getElementById("state-" + state).checked = true;
	if (state === "team-select") {
		let selectedTeam = document.querySelector("input[name='team']:checked");
		if (selectedTeam) onTeamChanged.call(selectedTeam);
	}
}

//======================================================================
//			Team select

function setupTeamSelect() {
	let redBtn = document.getElementById("team-red");
	redBtn.onchange = onTeamChanged;
	let blueBtn = document.getElementById("team-blue");
	blueBtn.onchange = onTeamChanged;
	let startBtn = document.getElementById("startgame");
	startBtn.onclick = function() {
		airconsole.message(0, {requestStartGame: true});
	};
}

function onTeamChanged() {
	switch (this.id) {
		case "team-red":
			airconsole.message(0, {team: "red"});
			updateBackgroundColor("red");
			break;
		case "team-blue":
			airconsole.message(0, {team: "blue"});
			updateBackgroundColor("blue");
			break;
	}
}

function setStartGameButtonEnabled(enabled) {
	document.getElementById("startgame").classList.toggle("enabled", enabled);
}

function startGame() {
	// Switch to "Controller" state
	setState("controller");
	// Make tutorial's Done button go to the controller instead of the team-select screen:
	document.getElementById("tut-done").setAttribute("for", "state-controller");
}

//======================================================================
//			D-pad

var dpad = {
	el: null,
	centerX: 0,
	centerY: 0,
	width: 0,
	dir: "",
};

function setupDPad() {
	dpad.el = document.getElementById("dpad");
	const rect = dpad.el.getBoundingClientRect();
	dpad.centerX = rect.left + rect.width/2;
	dpad.centerY = rect.top + rect.height/2;
	dpad.width = rect.width;
	dpad.el.addEventListener("touchstart", dpadTouchEvent, false);
	dpad.el.addEventListener("touchmove", dpadTouchEvent, false);
	dpad.el.addEventListener("touchend", () => setDPadActive(false));
	dpad.el.addEventListener("touchcancel", () => setDPadActive(false));
}

function dpadTouchEvent(e) {
	const minDistance = dpad.width / 5;

	const touch = e.changedTouches.item(0);
	const relX = touch.clientX - dpad.centerX;
	const relY = touch.clientY - dpad.centerY;
	const distance = Math.sqrt(relX*relX + relY*relY);
	const isActive = distance > minDistance;
	if (isActive) {
		updateDPad(relX, relY);
	}
	setDPadActive(isActive);
}

function setDPadActive(/*boolean*/isActive) {
	if (isActive) {
		dpad.el.classList.add("active");
	} else {
		dpad.el.classList.remove("active");
	}
}

// Degrees per radian, for conversion
const DEG_PER_RAD = 180 / Math.PI;

/**
 * Updates the D-pad state (i.e. direction) if necessary.
 * @param  {Number} x The X-coordinate of the touch relative to the D-pad center
 * @param  {Number} y The Y-coordinate of the touch relative to the D-pad center
 */
function updateDPad(x, y) {
	let degrees = Math.atan2(y, x) * DEG_PER_RAD;
	let newDir = Dir.fromDegrees(degrees);

	if (dpad.dir !== newDir) {
		// Send the new dir to the console:
		dpad.dir = newDir;
		airconsole.message(0, {dir: newDir});
	}
}

//======================================================================
//			Fire button

function setupFireButton() {
	const btnEl = document.getElementById("firebutton");

	btnEl.addEventListener("touchstart", function() {
		btnEl.classList.add("active");
		onFireButtonClicked();
	}, false);

	btnEl.addEventListener("touchend", function() {
		btnEl.classList.remove("active");
	}, false);
}

function onFireButtonClicked() {
	try {
		airconsole.message(0, {fire: 1, dir: dpad.dir});
	} catch (e) {
		console.error(e);
	}
}


//======================================================================
//			Background color

function updateBackgroundColor(color) {
	if (color === "red") {
		document.body.style.backgroundColor = "crimson";
	} else {
		document.body.style.backgroundColor = "mediumblue";
	}
}

//======================================================================
//			DEFEATED state
function setupGetHeroBtn() {
	document.getElementById("get-hero").onclick = () => {
		airconsole.getPremium();
	};
}

function onAcPremium() {
	document.getElementById("get-hero").remove();
}

//======================================================================
//			GAME OVER state

function setupGameover() {
	document.getElementById("again-same").onclick = onAgainSameBtnClicked;
	document.getElementById("again-change").onclick = onAgainChangeBtnClicked;
	document.getElementById("again-ready").onclick = onAgainReadyBtnClicked;
}

function onAgainSameBtnClicked() {
	airconsole.message(0, {again: "same"});
}

function onAgainChangeBtnClicked() {
	airconsole.message(0, {again: "change"});
}

function onAgainReadyBtnClicked() {
	airconsole.message(0, {again: true});
}

function showGameOver(/*Boolean*/ isMaster) {
	setState("gameover");
	// Show different controls for master player:
	document.querySelector("section.gameover")
			.classList.toggle("playerIsMaster", isMaster);
}
